using System;
using System.Collections.Generic;

namespace dfa
{
    /// <summary>
    /// Deterministic Finite Automaton.
    /// </summary>
    class DFA
    {
        /// <summary>
        /// Gets or Sets the set of all states.
        /// </summary>
        /// <value>Set of all states.</value>
        private int[] q { get; set; }

        /// <summary>
        /// Gets or Sets the set of all alphabets.
        /// </summary>
        /// <value>Set of all alphabets.</value>
        private char[] sigma { get; set; }

        /// <summary>
        /// Gets or Sets the transition function.
        /// </summary>
        /// <value>Transition Function.</value>
        private Dictionary<Tuple<int, char>, int> delta { get; set; }

        /// <summary>
        /// Gets or sets the initial state.
        /// </summary>
        /// <value>Initial state.</value>
        private int q0 { get; set; }

        /// <summary>
        /// Gets or Sets the set of final states.
        /// </summary>
        /// <value>Set of final states.</value>
        private int[] f { get; set; }

        /// <summary>
        /// Initializes a new instance (M) of the <see cref="dfa.DFA"/> class.
        /// </summary>
        /// <param name="Q">Set of all states (provided as an array of integers).</param>
        /// <param name="Sigma">Set of all alphabets (provided as an array of characters).</param>
        /// <param name="delta">Mapping function.</param>
        /// <param name="q0">The initial state.</param>
        /// <param name="F">Set of all final states (provided as an array of integers).</param>
        public DFA(int[] Q, char[] Sigma, Dictionary<Tuple<int, char>, int> delta, int q0, int[] F)
        {
            this.q = Q;
            this.sigma = Sigma;
            this.delta = delta;
            this.q0 = q0;
            this.f = F;
        }

        /// <summary>
        /// Determines if the string is recognized by the DFA.
        /// </summary>
        /// <param name="w">The string.</param>
        public bool recognizes(string w)
        {
            // store the current state
            int curr_state = q0;

            // for each character in the string
            for (int i = 0; i < w.Length; i++)
            {
                curr_state = transition(curr_state, w[i]);
            }
            
            // check if curr_state is in F
            return isFinalState(curr_state);
        }

        /// <summary>
        /// Perform the Transition with specified state and character.
        /// </summary>
        /// <param name="state">State.</param>
        /// <param name="c">C.</param>
        public int transition(int state, char c)
        {
            //  key: <state, c>
            Tuple<int, char> key = new Tuple<int, char>(state, c);
            if (delta.ContainsKey(key))
            {
                return delta[key];
            }
            // otherwise, return that it didn't work
            return -1;
        }

        /// <summary>
        /// Checks if the state is a final state.
        /// </summary>
        /// <returns><c>true</c>, if is a final state, <c>false</c> otherwise.</returns>
        /// <param name="state">State.</param>
        public bool isFinalState(int state)
        {
            // iterate over final states and see if it is in there
            foreach (int s in f)
            {
                if (state == s)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Print the description of this DFA (M).
        /// </summary>
        public void print()
        {
            // print the states
            Console.Write("States:  {");
            for (int i = 0; i < q.Length; i++)
            {
                Console.Write(" {0}", q [i]);
                // add a comma if not last element
                if (i != (q.Length - 1))
                {
                    Console.Write(",");
                }
            }
            Console.Write(" }\n\n");

            // print the alphabets
            Console.Write("Alphabets:  {");
            for (int j = 0; j < sigma.Length; j++)
            {
                Console.Write(" {0}", sigma [j]);
                // add a comma if not last element
                if (j != (sigma.Length - 1))
                {
                    Console.Write(",");
                }
            }
            Console.Write(" }\n\n");

            // print the transition function
            Console.Write("Transition Function: \n");
            // iterate over key-value pairs
            foreach (KeyValuePair<Tuple<int, char>, int> pair in delta)
            {
                Tuple<int, char> innerTuple = pair.Key;
                int innerTupleState = innerTuple.Item1;
                char innerTupleInput = innerTuple.Item2;
                int innerValue = pair.Value;
                Console.Write("({0}, {1}) --> {2}\n", innerTupleState, innerTupleInput, innerValue);
            }
            Console.Write("\n");

            // print the initial state
            Console.Write("Initial state: {0}\n\n", q0);

            // print the final states
            Console.Write("Final states =  {");
            for (int l = 0; l < f.Length; l++)
            {
                Console.Write(" {0}", f[l]);
                // add a comma if not last element
                if (l != (f.Length - 1))
                {
                    Console.Write(",");
                }
            }
            Console.Write(" }\n\n");
        }
    }
}
