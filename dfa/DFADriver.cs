using System;
using System.Collections.Generic;

namespace dfa
{
    /// <summary>
    /// A driver to test the output of the DFA class.
    /// </summary>
    public class DFADriver
    {
        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        public static void Main(string[] args)
        {
            int  [] states = {1, 2, 3 };
            int  [] F = {3};
            char [] ch = {'a', 'b'};
            
            Dictionary<Tuple<int, char>, int> d = new Dictionary<Tuple<int, char>, int>()
            {
                {new Tuple<int, char>(1, ch[0]), 2},
                {new Tuple<int, char>(1, ch[1]), 1},
                
                {new Tuple<int, char>(2, ch[0]), 2},
                {new Tuple<int, char>(2, ch[1]), 3},
                
                {new Tuple<int, char>(3, ch[0]), 3},
                {new Tuple<int, char>(3, ch[1]), 3},
            };
            
            DFA M = new DFA(states, ch, d, 1, F);
            M.print();

            string s = "aaaaab";
            Console.WriteLine("M {0} {1}", M.recognizes(s) ? "accepts" : "rejects", s); 
            
            s = "baa";
            Console.WriteLine("M {0} {1}", M.recognizes(s) ? "accepts" : "rejects", s);
        }
    }
}

