using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace dfa
{
    [TestFixture()]
    public class DFATests
    {
        private DFA M { get; set; }

        [SetUp()]
        public void Setup()
        {
            int  [] states = {1, 2, 3 };
            int  [] F = {3};
            char [] ch = {'a', 'b'};
            
            Dictionary<Tuple<int, char>, int> d = new Dictionary<Tuple<int, char>, int>()
            {
                {new Tuple<int, char>(1, ch[0]), 2},
                {new Tuple<int, char>(1, ch[1]), 1},
                
                {new Tuple<int, char>(2, ch[0]), 2},
                {new Tuple<int, char>(2, ch[1]), 3},
                
                {new Tuple<int, char>(3, ch[0]), 3},
                {new Tuple<int, char>(3, ch[1]), 3},
            };
            
            M = new DFA(states, ch, d, 1, F);
        }

        [TearDown()]
        public void TearDown()
        {
            M = null;
        }

        [Test()]
        public void TestDFAInitialized()
        {
            Assert.IsNotNull(M);
        }

        [Test()]
        public void TestAcceptedString()
        {
            Assert.IsTrue(M.recognizes("aaaaaaaaab"), "An appropriate string was denied.");
        }

        [Test()]
        public void TestDeniedString()
        {
            Assert.IsFalse(M.recognizes("baaaaaaaaa"), "An invalid string was accepted.");
        }
    }
}

